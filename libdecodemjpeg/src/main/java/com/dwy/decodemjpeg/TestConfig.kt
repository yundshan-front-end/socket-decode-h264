package com.dwy.decodemjpeg

object TestConfig {
    //本地测试模拟数据
    var ifCameraOpened = false//模拟摄像头已打开（打开命令已发送）
    var ifServerSendData = false//模拟服务器发送视频数据
    var enableConsoleLog = false//启用控制台日志
    var enableFileLog = false//启用日志写入文件
    var fileLogNewThread = true//启用日志写入文件是否单独一个线程
    var enableSourceDataRestore = false//启用源数据写入到文件
    var enableVideoDataRestore = false//启用视频数据写入到文件
    var showPicFrameIntervalTips = false//显示视频帧间隔提示
    var savePicFrame = false//保存视频帧
    var checkCrc = false//检测视频帧crc
    var enableBytes2HexLog = false//启用字节转十六进制日志
}